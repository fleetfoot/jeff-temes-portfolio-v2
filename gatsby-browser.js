/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

import "./src/css/index.css"
import "@fontsource/lora/700.css"
import "@fontsource/roboto/400.css"
import "@fontsource/roboto/700.css"
