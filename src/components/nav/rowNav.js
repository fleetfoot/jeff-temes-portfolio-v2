import * as React from "react"
import { Link } from "gatsby"

import "./nav.css"

const Nav = () => (
    <nav className="main-nav flex mb-8">
        <Link to="/" activeClassName="active" className="main-nav__item--row">
            Portfolio
        </Link>
        <Link to="/about" activeClassName="active" className="main-nav__item--row">
            About
        </Link>
        <Link to="/contact" activeClassName="active" className="main-nav__item--row">
            Contact
        </Link>
    </nav>
)

export default Nav
