const colors = require('tailwindcss/colors')

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    boxShadow: {
      inner: 'inset 0 -0.3em 0 0 #01401c',
    },
    colors: {
      "black": "#262626",
      "gold": "#b99e7e",
      "green": "#01401c",
      "offWhite": "#f2f2f2",
      "lightGray": "#d9dfdb",
      white: colors.white,
      transparent: colors.transparent,
    },
    fontFamily: {
      'body': ['Roboto', "sans-serif"],
      'headings': ['Lora', 'serif'],
    },
    screens: {
      "sm": "640px",
      "md": "768px",
      "lg": "960px",
    },

  },
  variants: {
    extend: {},
  },
  corePlugins: {
    container: false,
  },
  plugins: [],
}
