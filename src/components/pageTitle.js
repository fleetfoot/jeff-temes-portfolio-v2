import React from "react";

const PageTitle = props => <h1 className="mb-10 text-center">{props.children}</h1>;

export default PageTitle;
